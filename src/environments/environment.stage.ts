export const environment = {
  production: true,
  chatHostIpAddress: 'http://178.238.235.229:13000',
  userServiceIpAddress: 'http://178.238.235.229:18080/api/user/',
  postServiceIpAddress: 'http://178.238.235.229:18080/api/post/',
  authServiceIpAddress: 'http://178.238.235.229:18081/'
};
