export const environment = {
  production: true,
  chatHostIpAddress: 'http://178.238.235.229:3000',
  userServiceIpAddress: 'http://178.238.235.229:8080/api/user/',
  postServiceIpAddress: 'http://178.238.235.229:8080/api/post/',
  authServiceIpAddress: 'http://178.238.235.229:8081/'
};
