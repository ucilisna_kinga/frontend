
import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {environment} from './../../../../environments/environment';

import 'rxjs/add/operator/map';
import {Router} from "@angular/router";

@Injectable()
export class AuthTmpService {

//  private host = 'http://localhost:8080/';
  // private host =  'http://localhost:10000/api/user/';
  // private host = 'http://178.238.235.229:8081/';
  private host = environment.authServiceIpAddress;


  private userLoggedIn = false;
  private activeUser;
  private token;
  constructor(private http: Http, private _router: Router) {
    if (this.isUserLoggedIn()) {
      const username = this.getActiveUser().username;
      if (this._router.url === "/") {
        this._router.navigate([username, 'wall']);
      } else {
      }
    }
  }

  logInUser(username, password) {
    return this.http.post(this.host + 'auth', {
      username: username,
      password: password
    }).map(res => res.json()) ;
  }
  logOutUser() {
    this.userLoggedIn = false;
    this.clearToken();
  }

  isUsernameExist(username) {
    return this.http.get(this.host + `/auth/checkUsername/${username}`).map(res => res.json());
  }

  registerUser(user) {
    return this.http.put(this.host + 'auth/registerUser', user).map(res => res.json());
  }

  isUserLoggedIn() {
    return this.userLoggedIn ? true : this.userHaveToken();
  }

  userHaveToken() {
    if(localStorage.getItem('token'))
      return true;
    else
      return false;
  }

  setUserLoggedIn(value: boolean) {
    this.userLoggedIn = value;
  }

  getActiveUser() {
    if (this.activeUser)
      return this.activeUser;
    else
      return JSON.parse(localStorage.getItem('activeUser'));
  }

  setActiveUser(user) {
    this.activeUser = user;
    localStorage.setItem('activeUser', JSON.stringify(user));
  }
  getToken() {
    // return this.token;
    return localStorage.getItem('token');
  }
  setToken(t) {
    this.token = t;
    localStorage.setItem('token', t);
  }
  clearToken() {
    localStorage.removeItem('token');
    localStorage.removeItem('activeUser');
  }

}
