import {Injectable} from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from './../../../environments/environment';
import {AuthTmpService} from 'app/core/services/auth/auth-tmp-service';


@Injectable()
export class UploadFileService {

  fileNameUpload = '';
  fileUploaded = false;

  private host = environment.postServiceIpAddress;

  constructor(private http: HttpClient, private authTmpService: AuthTmpService) {}

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();

    formdata.append('file', file);

    let req = new HttpRequest('POST', this.host + 'uploadFile', formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    req = req.clone({
      setHeaders: {
        Authorization: `${this.authTmpService.getToken()}`
      }
    });
    return this.http.request(req);
  }

  // getFiles(): Observable<string[]> {
  //   return this.http.get('/getallfiles')
  // }
}
