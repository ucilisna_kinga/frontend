import {Component, EventEmitter, Output} from '@angular/core';
import {forEach} from "@angular/router/src/utils/collection";
import { HttpClient, HttpResponse, HttpEventType } from '@angular/common/http';
import {UploadFileService} from "app/core/services/upload-file.service";

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent {

  @Output() createdPost = new EventEmitter();
  postText = '';
  activePost = true;

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };

  constructor(private uploadService: UploadFileService) {
    this.uploadService.fileUploaded = false;
    this.uploadService.fileNameUpload = '';
  }

  selectFile(event) {
    this.uploadService.fileUploaded = false;
    this.uploadService.fileNameUpload = '';

    const file = event.target.files.item(0)

    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('invalid format!');
    }
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0)
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
          let name = JSON.stringify(event.body);
          name = name.substr(1);
          name = name.slice(0, -1);
          if(!name.startsWith("FAIL")) {
            this.uploadService.fileUploaded = true;
            this.uploadService.fileNameUpload = name;

            console.log('File is completely uploaded!');
            console.log('name: ' + name);
        }
      }
    });

    this.selectedFiles = undefined
  }

  createPost(postText) {
    this.activePost = false;
    let haveEmptyNewLines = true;
    const lines = postText.split(/\r\n|\r|\n/g);

    for (const l of lines) {
      if (l.length > 0 ) {
        haveEmptyNewLines = false;
      }
    }

    if (postText.length > 0 && !haveEmptyNewLines) {
      this.createdPost.emit({text: postText, fileName: this.uploadService.fileNameUpload});
      this.postText = '';
    }

  }
}
