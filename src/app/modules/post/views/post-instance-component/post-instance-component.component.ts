import {Component, EventEmitter, Input, Output} from '@angular/core';
import {environment} from './../../../../../environments/environment';


@Component({
  selector: 'app-post-instance-component',
  templateUrl: './post-instance-component.component.html',
  styleUrls: ['./post-instance-component.component.css']
})
export class PostInstanceComponentComponent {

  @Input() activeUser;
  @Input() wallUser;
  @Input() post;
  @Input() creatorOfPost;
  @Input() animation = false;
  @Output() postForUpdateLikes = new EventEmitter();
  constructor() { }

  resolvePicturePath() {
    if(this.post.picture) {
      return environment.postServiceIpAddress + 'files/' +  this.post.picture;
    }
  }

  emitPostForUpdateLikes(p, a) {
    this.postForUpdateLikes.emit({post: p, action: a});
  }

  canLikePost() {
    const res = !(this.post.likes.indexOf(this.activeUser.username) > -1);
    return res;
  }
}
