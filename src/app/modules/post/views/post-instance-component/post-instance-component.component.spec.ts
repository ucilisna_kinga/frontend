import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostInstanceComponentComponent } from './post-instance-component.component';

describe('PostInstanceComponentComponent', () => {
  let component: PostInstanceComponentComponent;
  let fixture: ComponentFixture<PostInstanceComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostInstanceComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostInstanceComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
