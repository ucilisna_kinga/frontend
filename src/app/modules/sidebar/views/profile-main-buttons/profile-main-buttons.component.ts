import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-profile-main-buttons',
  templateUrl: './profile-main-buttons.component.html',
  styleUrls: ['./profile-main-buttons.component.css']
})
export class ProfileMainButtonsComponent implements OnInit {

  @Input() user;
  @Input() isActiveUser;
  @Input() friends = true;
  @Output() sendMessage = new EventEmitter();
  @Output() addFriend = new EventEmitter();
  proposalSent = false;

  constructor( private route: ActivatedRoute ) { }

  ngOnInit() {
    // this.route.params.subscribe(params => {
    //   this.proposalSent = false;
    //   console.log('this.proposalSent-z3');
    //   console.log(this.proposalSent);
    // });
  }

  onClickSendMessage() {
    this.sendMessage.emit();
  }

  onClickAddFriend() {
    this.proposalSent = true;
    this.addFriend.emit(this.user);
  }
}
