import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-chat-message-line',
  templateUrl: './chat-message-line.component.html',
  styleUrls: ['./chat-message-line.component.css']
})
export class ChatMessageLineComponent implements OnInit {

  @Input() message;
  @Input() activeUser;
  @Input() animation = false;

  constructor() { }

  ngOnInit() {
  }

}
