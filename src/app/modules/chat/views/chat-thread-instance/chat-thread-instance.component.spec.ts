import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatThreadInstanceComponent } from './chat-thread-instance.component';

describe('ChatThreadInstanceComponent', () => {
  let component: ChatThreadInstanceComponent;
  let fixture: ComponentFixture<ChatThreadInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatThreadInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatThreadInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
