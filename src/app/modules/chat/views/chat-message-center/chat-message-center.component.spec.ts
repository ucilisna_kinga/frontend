import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatMessageCenterComponent } from './chat-message-center.component';

describe('ChatMessageCenterComponent', () => {
  let component: ChatMessageCenterComponent;
  let fixture: ComponentFixture<ChatMessageCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChatMessageCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatMessageCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
