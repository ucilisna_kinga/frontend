import {Component, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-chat-message-center',
  templateUrl: './chat-message-center.component.html',
  styleUrls: ['./chat-message-center.component.css']
})
export class ChatMessageCenterComponent {

  textArea = '';
  @Input() activeUser;
  @Input() messagesByThread = [];
  @Output() messageSent = new EventEmitter();

  constructor() {
  }


  // getMessagesForActiveThread() {
  //   const activeThread = this._threadTmpService.getActiveThread();
  //   const messagesByThread = this._messagesTmpService.getMessagesByThread(activeThread);
  //   return messagesByThread;
  // }

  // sendMessage(text) {
  //   let haveEmptyNewLines = true;
  //   const lines = text.split(/\r\n|\r|\n/g);
  //
  //   for (const l of lines) {
  //     if (l.length > 0 ) {
  //       haveEmptyNewLines = false;
  //     }
  //   }
  //
  //   if (text.length > 0 && !haveEmptyNewLines) {
  //     this._messagesTmpService.createMessage(text, this.activeUser.username, this._threadTmpService.getActiveThread().id );
  //     this.textArea = '';
  //   }
  // }

  sendMessage(text) {
    this.messageSent.emit(text);
    this.textArea = '';
  }


}
