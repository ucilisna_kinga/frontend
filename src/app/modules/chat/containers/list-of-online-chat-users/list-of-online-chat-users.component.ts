import {ChangeDetectorRef , Component, OnInit, Input} from '@angular/core';
import {AuthTmpService} from '../../../../core/services/auth/auth-tmp-service';
import {ChatService} from '../../../../store/data-services/chat-service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-of-online-chat-users',
  templateUrl: './list-of-online-chat-users.component.html',
  styleUrls: ['./list-of-online-chat-users.component.css']
})
export class ListOfOnlineChatUsersComponent implements OnInit {

  activeUser;
  activeUserFriends;
  collapseStatus = false;

  constructor(private _chatService: ChatService,
              private _cdRef: ChangeDetectorRef,
              private _router: Router,
              private _authTmpService: AuthTmpService) {
    // set tem on chat service on userfirends endpoint and get them here form chat service
    this.activeUser = this._authTmpService.getActiveUser();
    this.activeUserFriends = this._chatService.onlineUserFriendsForActiveUser;
  }

  ngOnInit() {
    this._chatService.socket.emit('reqGetAllActiveUsers', this.activeUser.username);

    this._chatService.socket.on('newUserIsOnline', username => {
      this._chatService.socket.emit('reqGetAllActiveUsers', this.activeUser.username);
    });

    this._chatService.socket.on('userOffline', username => {
      this._chatService.socket.emit('reqGetAllActiveUsers', this.activeUser.username);
    });

    this._chatService.socket.on('resGetAllActiveUsers', (allActiveFriends) => {
      this._chatService.onlineUserFriendsForActiveUser = allActiveFriends;
      this.activeUserFriends = allActiveFriends;
    });
  }

  onlineUserClicked(f) {
    this._router.navigate([f.username, 'wall']);
  }

  onCollapse() {
    if (this.collapseStatus) {
      this.collapseStatus = false;
    } else {
      this.collapseStatus = true;
    }
  }
}
