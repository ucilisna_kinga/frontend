import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthTmpService} from '../../../core/services/auth/auth-tmp-service';
import {ChatService} from '../../../store/data-services/chat-service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  typeProfile;
  showUsernameError = false;

  constructor(private _router: Router,
              private _chatService: ChatService,
              private _authTmpService: AuthTmpService) { }

  setTypeProfile(value) {
    this.typeProfile = value;
  }

  createTeacherProfile( regUsername, regPassword, regFirstname, regLastname, regGenderMale, regSchool) {
    this._authTmpService.isUsernameExist(regUsername).toPromise().then(res => {
       if (res.message === 'true') {
         this.showUsernameError = true;
       } else {
         this.showUsernameError = false;
         const user = {
           username: regUsername, password: regPassword, firstName: regFirstname, lastName: regLastname,
           school: regSchool, parentFor: '', gender: this.getGender(regGenderMale), profileType: 'teacher',
           avatarUrl: this.getAvatarUrl('teacher', this.getGender(regGenderMale))};
         this._authTmpService.registerUser(user).toPromise().then( u => {
            this._authTmpService.logInUser(user.username, user.password).toPromise().then( r => {
              if (r.message) {
                this._authTmpService.setUserLoggedIn(false);
              } else if (r.token) {
                this.setupUserAndConnection(r.token, u);
              }
            });
         });
       }
    });
  }

  createStudentProfile( regUsername, regPassword, regFirstname, regLastname, regGenderMale) {

    this._authTmpService.isUsernameExist(regUsername).toPromise().then(res => {
      if (res.message === 'true') {
        this.showUsernameError = true;
      } else {
        this.showUsernameError = false;
        const user = {
          username: regUsername, password: regPassword, firstName: regFirstname, lastName: regLastname,
          school: '', parentFor: '', gender: this.getGender(regGenderMale), profileType: 'student',
          avatarUrl: this.getAvatarUrl('student', this.getGender(regGenderMale))};
        this._authTmpService.registerUser(user).toPromise().then( u => {
          this._authTmpService.logInUser(user.username, user.password).toPromise().then( r => {
            if (r.message) {
              this._authTmpService.setUserLoggedIn(false);
            } else if (r.token) {
              this.setupUserAndConnection(r.token, u);
            }
          });
        });
      }
    });
  }

  createParentProfile( regUsername, regPassword, regFirstname, regLastname, regGenderMale, regParentFor) {

    this._authTmpService.isUsernameExist(regUsername).toPromise().then(res => {
      if (res.message === 'true') {
        this.showUsernameError = true;
      } else {
        this.showUsernameError = false;
        const user = {
          username: regUsername, password: regPassword, firstName: regFirstname, lastName: regLastname,
          school: '', parentFor: regParentFor, gender: this.getGender(regGenderMale), profileType: 'parent',
          avatarUrl: this.getAvatarUrl('parent', this.getGender(regGenderMale))};
        this._authTmpService.registerUser(user).toPromise().then( u => {
          this._authTmpService.logInUser(user.username, user.password).toPromise().then( r => {
            if (r.message) {
              this._authTmpService.setUserLoggedIn(false);
            } else if (r.token) {
              this.setupUserAndConnection(r.token, u);
            }
          });
        });
      }
    });
  }

  getGender(regGenderValue) {
    if (regGenderValue) {
      return 'male';
    } else {
      return 'female';
    }
  }

  getAvatarUrl(profileType, gender) {
    return `${profileType}_${gender}.jpg`;
  }

  setupUserAndConnection(token , user) {
    this._authTmpService.setToken(token);
    this._authTmpService.setActiveUser(user);
    this._authTmpService.setUserLoggedIn(true);
    this._chatService.openSocket(user.username);
    this._router.navigate([user.username, 'wall']);
  }

}
