
import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-middle-icons',
  templateUrl: './middle-icons.component.html',
  styleUrls: ['./middle-icons.component.css']
})
export class MiddleIconsComponent implements OnInit {

  @Input() numOfUnreadedThreads = 0;
  @Input() numOfFriendRequests = 0;
  @Input() user;
  @Output() midIconsCompStartInit = new EventEmitter;
  @Output() friendsRequestIconClicked = new EventEmitter;
  @Output() friendRequestDeselect = new EventEmitter;

  ngOnInit() {
    this.midIconsCompStartInit.emit();
  }
  onFriendsRequestIconClicked() {
    this.friendsRequestIconClicked.emit();
  }

}
