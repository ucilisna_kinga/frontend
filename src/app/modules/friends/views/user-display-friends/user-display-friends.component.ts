import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-user-display-friends',
  templateUrl: './user-display-friends.component.html',
  styleUrls: ['./user-display-friends.component.css']
})
export class UserDisplayFriendsComponent implements OnInit {

  @Input() user;
  @Output() userSelected = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onUserSelected() {
    this.userSelected.emit();
  }

  clicked() {
    console.log('otkazi');
  }

}
