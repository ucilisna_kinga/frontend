import {Component, OnInit, OnDestroy} from '@angular/core';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {UserService} from '../../store/data-services/user.service';
import {PostService} from '../../store/data-services/post.service';

@Component({
  selector: 'app-profile-news-feed-page',
  templateUrl: './profile-news-feed-page.component.html',
  styleUrls: ['./profile-news-feed-page.component.css']
})
export class ProfileNewsFeedPageComponent implements OnInit, OnDestroy {

  public activeUser;
  public wallUser;
  public newsFeedsPosts;
  loading = { doneLoadingUser: false, doneLoadingPostsWithUsers: false };
  animateFirstElement;
  private allPostsFromFriends = [];
  private allActiveUserFriends = [];
  getPostsFromFriendsIntervalId = 1;

  constructor(private authTmpService: AuthTmpService,
              private userService: UserService,
              private router: Router,
              public postService: PostService,
              private route: ActivatedRoute) {
    this.activeUser = this.authTmpService.getActiveUser();
  }

  isUserLoggedIn() {
    return this.authTmpService.isUserLoggedIn();
  }

  ngOnInit() {
    this.loading = { doneLoadingUser: false, doneLoadingPostsWithUsers: false };
    this.allPostsFromFriends = [];
    this.allActiveUserFriends = [];
    this.newsFeedsPosts = [];
    this.animateFirstElement = true;
    // this.router.events.subscribe(() => {
    //   console.log('route changed ');
    //   this.doneLoadingUser = false;
    //   this.doneLoadingPostsWithUsers = false;
    //
    //   this.allPostsFromFriends = [];
    //   this.allActiveUserFriends = [];
    // });

    this.route.params.forEach((params: Params) => {
      this.userService.getUserByUsername(params['username'])
        .subscribe(u => {
          this.loading.doneLoadingUser = true;
          if (u.username == null) {
            console.log('user not exist - display div');
          } else {
            this.wallUser = u;
          }
        });

      this.getPostsFromFriends();
      this.getPostsFromFriendsIntervalId = setInterval( this.getPostsFromFriends.bind(this), 2000);
    });
  }

  ngOnDestroy() {
    clearInterval(this.getPostsFromFriendsIntervalId);
  }

  getPostsFromFriends() {
    this.userService.getUserFriendsByUsername(this.authTmpService.getActiveUser().username)
      .toPromise().then(ufs => {
        this.allActiveUserFriends = ufs;
        let allPostsPromises = [];
        ufs.forEach( u => {
          allPostsPromises.push(this.postService.getAllPostByUsername(u.username));
        });

        Promise.all(allPostsPromises).then( pss => {
          const tmpAllPostsFromFriends = [];
          pss.forEach( ps => {
            if (ps.length === 1 ) {
              tmpAllPostsFromFriends.push(ps[0]);
            } else if (ps.length > 1 ) {
              ps.forEach( p => tmpAllPostsFromFriends.push(p) );
            }
          });

          if (this.userService.oldAllPostsFromFriendsLength !== tmpAllPostsFromFriends.length) {
            this.animateFirstElement = true;
          }
          this.userService.oldAllPostsFromFriendsLength = tmpAllPostsFromFriends.length;
          this.allPostsFromFriends = tmpAllPostsFromFriends;

          this.postService.sortPostsByDate(this.allPostsFromFriends);
          this.postService.addCreatedByUserToPosts(this.allActiveUserFriends, this.allPostsFromFriends);
          this.loading.doneLoadingPostsWithUsers = true;
          this.newsFeedsPosts = this.allPostsFromFriends;

        });
      });
  }

  onCreatedPost(post) {
    this.postService.savePost(this.activeUser, post)
      .subscribe( p => {
        this.router.navigate([this.activeUser.username, 'wall']);
      });
    // this._postTmpService.createWallPost(this.activeUser, $event);
  }
  updatePostLikes(post) {
    this.postService.updatePostLikes(this.activeUser, post)
      .subscribe( d => {
        console.log('d');
        console.log(d);
      });
  }



}
