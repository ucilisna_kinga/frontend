import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileMessagesPageComponent } from './profile-messages-page.component';

describe('ProfileMessagesPageComponent', () => {
  let component: ProfileMessagesPageComponent;
  let fixture: ComponentFixture<ProfileMessagesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileMessagesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileMessagesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
