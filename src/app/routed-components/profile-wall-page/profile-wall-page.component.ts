import {AfterViewInit, Component, OnInit, OnDestroy, ChangeDetectionStrategy} from '@angular/core';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import {ActivatedRoute, Params } from '@angular/router';
import {UserService} from '../../store/data-services/user.service';
import {PostService} from '../../store/data-services/post.service';
import {UiStateService} from '../../core/services/ui-state.service';

@Component({
  selector: 'app-profile-wall-page',
  templateUrl: './profile-wall-page.component.html',
  styleUrls: ['./profile-wall-page.component.css']
})
export class ProfileWallPageComponent implements OnInit, OnDestroy {

  public activeUser;
  public wallUser;
  public wallPosts;
  displayPostsFirstTime;
  doneLoadingUser;
  doneLoadingPosts;
  getAllWallUserPostsIntervalId;

  constructor(private authTmpService: AuthTmpService,
              private userService: UserService,
              private postService: PostService,
              private route: ActivatedRoute) {
    this.activeUser = this.authTmpService.getActiveUser();
  }

  isUserLoggedIn() {
    return this.authTmpService.isUserLoggedIn();
  }

  ngOnInit() {
    this.displayPostsFirstTime = true;
    this.doneLoadingUser = false;
    this.doneLoadingPosts = false;
    this.route.params.forEach((params: Params) => {
      this.userService.getUserByUsername(params['username'])
        .subscribe(u => {
          this.doneLoadingUser = true;
          if (u.username == null) {
            console.log('user not exist - display div');
          } else {
            this.wallUser = u;
          }

          this.getAllWallUserPosts();
          this.getAllWallUserPostsIntervalId = setInterval( this.getAllWallUserPosts.bind(this), 2000);

        });
    });
  }

  ngOnDestroy() {
    clearInterval(this.getAllWallUserPostsIntervalId);
  }

  getAllWallUserPosts() {
    let username;
    if(this.activeUser.username === this.wallUser.username) {
      username = this.activeUser.username;
    } else {
      username = this.wallUser.username;
    }

    this.postService.getAllPostByUsername(username)
      .then( ps => {
        this.postService.sortPostsByDate(ps);
        ps.forEach( p => {
          p.creatorOfPost = this.wallUser;
          p.timeCreated = this.postService.convertTime(p.timeCreated);
        });

        this.wallPosts = ps;
        this.doneLoadingPosts = true;
      });
  }

  onCreatedPost(post) {
    this.displayPostsFirstTime = false;
    if (post.text.length > 0) {
      this.postService.savePost(this.activeUser, post)
        .subscribe( () => {
          this.getAllWallUserPosts();
        });
    }

  }

  canCreatePost() {
    if ( this.wallUser.username === this.activeUser.username ) { return true; } else {
      return false;
    }
  }

  updatePostLikes(payload) {
    this.postService.updatePostLikes(this.activeUser, payload)
      .subscribe( d => {
        console.log('d');
        console.log(d);
      });
  }

}
