import { Component, ChangeDetectionStrategy } from '@angular/core';
import {BookTmpService} from '../../../store/data-services/book-tmp-service';
import {Store} from '@ngrx/store';
import * as fromRoot from '../../../store/reducers/index';
import * as book from '../../../store/actions/book-actions';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {Book} from '../../../store/models/db-models/book';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-home-page',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './book-home-page.component.html',
  styleUrls: ['./book-home-page.component.css']
})
export class BookHomePageComponent {
  books$;
  selectedBook$: Observable<Book>;


  constructor(private bookTmpService: BookTmpService,
              private store: Store<fromRoot.State>) {
    this.books$ = store.select(fromRoot.getBooksState)
      .map( (i) => (i.booksFromSearch));

    this.selectedBook$ = store.select(fromRoot.getBooksState)
      .map( (i) => (i.selectedBook) );
  }

  searchBooks($event) {
      // this.books$ = this.bookTmpService.searchBooks($event);
    this.store.dispatch(new book.SearchAction($event));
  }
}
