import {Component} from '@angular/core';

@Component({
  selector: 'app-sing-in',
  templateUrl: 'sing-in-page.component.html',
  styleUrls: ['sing-in-page.component.css']
})
export class SingInPageComponent {
  showSpinner = false;
  constructor() {}

  onSpinnerValue(val) {
    if (val === 'start') {
      this.showSpinner = true;
    } else if (val === 'stop'){
      this.showSpinner = false;
    }
  }
}
