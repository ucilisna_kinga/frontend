import {Injectable} from '@angular/core';

@Injectable()
export class MessageTmpService {

  messages = [
    {
      id: 0,
      text: 'm1-From Jovan',
      sendByUsername: 'jshalamanoski',
      isReaded: false,
      isDeleted: false,
      forThreadId: 0
    },
    {
      id: 1,
      text: 'm1-From Jovan',
      sendByUsername: 'jshalamanoski',
      isReaded: false,
      isDeleted: false,
      forThreadId: 1
    },
    {
      id: 2,
      text: 'm2 from teacherFemale',
      sendByUsername: 'teacherFemale',
      isReaded: false,
      isDeleted: false,
      forThreadId: 1
    },
    {
      id: 3,
      text: 'm2 from teacherFemale',
      sendByUsername: 'teacherFemale',
      isReaded: false,
      isDeleted: false,
      forThreadId: 0
    },
    {
      id: 4,
      text: 'message1',
      sendByUsername: 'jshalamanoski',
      isReaded: false,
      isDeleted: false,
      forThreadId: 2
    },
    {
      id: 5,
      text: 'message1',
      sendByUsername: 'jshalamanoski',
      isReaded: false,
      isDeleted: false,
      forThreadId: 3
    },
    {
      id: 6,
      text: 'message2',
      sendByUsername: 'studentFemale',
      isReaded: false,
      isDeleted: false,
      forThreadId: 2
    },
    {
      id: 7,
      text: 'message2',
      sendByUsername: 'studentFemale',
      isReaded: false,
      isDeleted: false,
      forThreadId: 3
    }
  ];


  getMessagesByThread(thread) {
    const messagesByThread = [];
    for (const m of this.messages) {
      if (m.forThreadId === thread.id) {
        messagesByThread.push(m);
      }
    }
    // console.log(messagesByThread);
    return messagesByThread;
  }

  createMessage(text, sendByUsername, threadId) {
    this.messages.push({
      id: this.messages.length,
      text: text,
      sendByUsername: sendByUsername,
      forThreadId: threadId,
      isReaded: false,
      isDeleted: false,
    });
  }

}
