import {Injectable} from '@angular/core';

@Injectable()
export class PostTmpService {

  /*
   {
   id: '0',
   text: 'Ова е прв генериран НАСТАН',
   createdByUsername: 'jshalamanoski',
   timeCreated: '09 Мај 20:15 -п',
   forUsername: 'jshalamanoski'
   }
   */
  newsFeedsPosts = [
    {
      id: '0',
      text: 'Ова е прв генериран НАСТАН',
      createdByUsername: 'jshalamanoski',
      timeCreated: '09 Мај 20:15 -п',
      forUsername: 'teacherMale'
    },
    {
      id: '1',
      text: 'Ова е прв генериран НАСТАН',
      createdByUsername: 'jshalamanoski',
      timeCreated: '09 Мај 20:15 -п',
      forUsername: 'teacherFemale'
    },
    {
      id: '2',
      text: 'Ова е прв генериран НАСТАН',
      createdByUsername: 'jshalamanoski',
      timeCreated: '09 Мај 20:15 -п',
      forUsername: 'studentMale'
    },
    {
      id: '3',
      text: 'Ова е прв генериран НАСТАН',
      createdByUsername: 'jshalamanoski',
      timeCreated: '09 Мај 20:15 -п',
      forUsername: 'studentFemale'
    },
    {
      id: '4',
      text: 'Ова е прв генериран НАСТАН',
      createdByUsername: 'jshalamanoski',
      timeCreated: '09 Мај 20:15 -п',
      forUsername: 'parentMale'
    },
    {
      id: '5',
      text: 'Ова е прв генериран НАСТАН',
      createdByUsername: 'jshalamanoski',
      timeCreated: '09 Мај 20:15 -п',
      forUsername: 'parentFemale'
    }
  ];

//   two different tables because partisioning to be done correctly
//   newsFeedsPosts partisioned by forUsername
//   wallPosts partisioned by username
  /*
   {
   id: '0',
   text: 'Ова е прв генериран пост',
   createdByUsername: 'jshalamanoski',
   timeCreated: '09 Мај 20:15 -п'
   },
   {
   id: '1',
   text: 'Ова е втор генериран пост',
   createdByUsername: 'jshalamanoski',
   timeCreated: '09 Мај 20:15 -п'
   }
   */
  wallPosts = [{
    id: '0',
    text: 'Ова е прв генериран пост',
    createdByUsername: 'jshalamanoski',
    timeCreated: '09 Мај 20:15 -п'
  }];

  getAllWallPostForUsername(username) {
    const userPosts = [];
    for ( const p of this.wallPosts) {
      if ( p.createdByUsername === username ) { userPosts.push(p); }
    }
    return userPosts;
  }

  createWallPost(user, text) {
    this.wallPosts.unshift(
      {id: this.wallPosts.length.toString(),
        text: text, createdByUsername: user.username, timeCreated: new Date().toString().slice( 0, 21 )}
      );
    this.createNewsFeedPost(user, text);
  }


  getAllNewsFeedsPostsForUsername(username) {
    const userNewsFeedsPosts = [];
    for (const nfp of this.newsFeedsPosts) {
      if ( nfp.forUsername === username ) {
        userNewsFeedsPosts.push(nfp);
      }
    }
    return userNewsFeedsPosts;
  }

  private createNewsFeedPost(user, text) {
    const date = new Date().toString().slice( 0, 21 );

    for (const un of user.friendsUsernames) {
      this.newsFeedsPosts.unshift({
          id: this.newsFeedsPosts.length.toString(),
          text: text,
          createdByUsername: user.username,
          timeCreated: date,
          forUsername: un
        });
    }
  }


}
