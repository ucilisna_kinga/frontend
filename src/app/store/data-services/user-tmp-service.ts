import {Injectable} from '@angular/core';

@Injectable()
export class UserTmpService {

  users = [
    {
      id: '1',
      username: 'jshalamanoski',
      password: 'passjshalamanoski',
      firstName: 'Јован',
      lastName: 'Шаламаноски',
      school: '',
      parentFor: '',
      gender: 'male',
      friendsUsernames: ['teacherFemale', 'studentMale', 'studentFemale', 'parentMale', 'parentFemale'],
      profileType: 'teacher',
      avatarUrl: 'teacher_male.jpg'
    },
    {
      id: '2',
      username: 'teacherFemale',
      password: 'passteacherFemale',
      firstName: 'УчителкаИме',
      lastName: 'УчителкаПрезиме',
      school: '',
      parentFor: '',
      gender: 'female',
      friendsUsernames: ['jshalamanoski', 'studentMale', 'studentFemale', 'parentMale', 'parentFemale'],
      profileType: 'teacher',
      avatarUrl: 'teacher_female.jpg'
    },
    {
      id: '3',
      username: 'studentMale',
      password: 'passstudentMale',
      firstName: 'УченикИме',
      lastName: 'УченикПрезиме',
      school: '',
      parentFor: '',
      gender: 'male',
      friendsUsernames: ['jshalamanoski', 'studentFemale', 'parentMale', 'parentFemale'],
      profileType: 'student',
      avatarUrl: 'student_male.jpg'
    },
    {
      id: '4',
      username: 'studentFemale',
      password: 'studentFemale',
      firstName: 'УченичкаИме',
      lastName: 'УченичкаПрезиме',
      school: '',
      parentFor: '',
      gender: 'female',
      friendsUsernames: ['jshalamanoski', 'studentMale', 'parentMale', 'parentFemale'],
      profileType: 'student',
      avatarUrl: 'student_female.jpg'
    },
    {
      id: '5',
      username: 'parentMale',
      password: 'passparentMale',
      firstName: 'РодителИме',
      lastName: 'РодителПрезиме',
      school: '',
      parentFor: '',
      gender: 'male',
      friendsUsernames: ['jshalamanoski', 'teacherFemale', 'parentFemale'],
      profileType: 'parent',
      avatarUrl: 'parent_male.jpg'
    },
    {
      id: '6',
      username: 'parentFemale',
      password: 'passparentFemale',
      firstName: 'РодителкаИме',
      lastName: 'РодителкаПрезиме',
      school: '',
      parentFor: '',
      gender: 'female',
      friendsUsernames: ['jshalamanoski', 'teacherFemale', 'parentMale'],
      profileType: 'parent',
      avatarUrl: 'parent_female.jpg'
    }
  ];
  getUsers() {
    return this.users;
  }

  insertUser(user) {
    if (!this.userExist(user.username)) {
      user.id = this.users.length + 1;
      this.users.push(user);
      console.log(this.users);
      return true;
    } else {
      console.log('user exist');
      return false;
    }
  }


  getUserFriends(user) {
    const userFriends = [];
    let userFriendsUsernames = [];
    for (const u of this.users) {
      if (u.username === user.username) {
        userFriendsUsernames = u.friendsUsernames;
      }
    }

    for (const u of this.users) {
      for (const f of userFriendsUsernames) {
        if (u.username === f) {
          userFriends.push(u);
        }
      }
    }
    return userFriends;
  }

  userExist(username) {
    for ( const u of this.users ) {
      if (u.username === username) {
        return true;
      }
    }
    return false;
  }

  getUserByUsername(username) {
    for ( const u of this.users ) {
      if (u.username === username) {
        return u;
      }
    }
    return this.users[0];
  }

  searchUsers(term) {
    const listOfSearchedUsers = [];
    for (const u of this.users) {
      if (u.username.toLowerCase().startsWith(term.toLowerCase()) ||
        u.firstName.toLowerCase().startsWith(term.toLowerCase()) ||
        u.lastName.toLowerCase().startsWith(term.toLowerCase()) ) {
          listOfSearchedUsers.unshift(u);
      }
    }
    return listOfSearchedUsers;
  }


}
