import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from './../../../environments/environment';
import {AuthTmpService} from "../../core/services/auth/auth-tmp-service";

@Injectable()
export class ChatService {

  public inMessagesRoute = false;
  public socket = null;
  public onlineUserFriendsForActiveUser = [];

  constructor(private authTmpService: AuthTmpService) {
    if (this.authTmpService.isUserLoggedIn()) {
      const username = this.authTmpService.getActiveUser().username;
      this.openSocket(username);
    }
  }

  public openSocket(username) {
    this.socket = io(environment.chatHostIpAddress,  { query: `username=${username}` });
    // console.log('envir_chat_ip');
    // console.log(environment.chatHostIpAddress);

  }
  public closeSocket() {
    this.socket.disconnect();
  }
}
