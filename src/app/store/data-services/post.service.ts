import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import {environment} from './../../../environments/environment';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class PostService {

  lastPostsForWallUser = [];

  // private url =  'http://localhost:8082/';
  // private url =  'http://localhost:8080/api/post/';
  // private url =  'http://178.238.235.229:8080/api/post/';
  private url =  environment.postServiceIpAddress;
  constructor(private http: Http, private authTmpService: AuthTmpService) { }

  getAllPostByUsername(username) {
    return this.http.get(this.url + `posts/${username}`, { headers: this.getHeaders(this.authTmpService.getToken())})
      .map(res => res.json()).toPromise();
  }

  savePost(user, p) {
    const post = {
      id: 'tmpUUID',
      text: p.text,
      createdByUsername: user.username,
      timeCreated: new Date().getTime().toString(),
      picture: p.fileName,
      likes: []
    };
    return this.http.put(this.url + `post`, post, { headers: this.getHeaders(this.authTmpService.getToken())})
      .map(res => res.json());
  }

  updatePostLikes(user, payload) {
    console.log('user');
    console.log(user);
    console.log(payload);
    let post = {
      id: payload.post.id,
      text: payload.post.text,
      createdByUsername: payload.post.createdByUsername,
      timeCreated: payload.post.rawTimeCreated,
      picture: payload.post.picture,
      likes: payload.post.likes
    };
    if(payload.action === 'like') {
      post.likes.push(user.username);
      return this.http.post(this.url + `post`, post, { headers: this.getHeaders(this.authTmpService.getToken())})
        .map(res => res.json());
    } else if(payload.action === 'unlike') {
      post.likes.pop(user.username);
      return this.http.post(this.url + `post`, post, { headers: this.getHeaders(this.authTmpService.getToken())})
        .map(res => res.json());
    }
  }

  getHeaders(token) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    return headers;
  }


  sortPostsByDate(posts) {
    posts.sort(function (a, b) {
      return b.timeCreated - a.timeCreated;
    });
  }

  addCreatedByUserToPosts(users, posts) {
    posts.forEach(p => {
      p.rawTimeCreated = p.timeCreated;
      p.timeCreated = this.convertTime(p.timeCreated);
      p.creatorOfPost = this.getPostCreator(p.createdByUsername, users);
    });
  }

  getPostCreator(username, users) {
    let returnedUser;
    for (let i = 0; i < users.length; i++) {
      const u = users[i];
      if (u.username === username) {
        returnedUser = u;
        break;
      }
    }
    return returnedUser;
  }

  convertTime(time) {
    const t = parseInt(time);
    const r = new Date(t).toString().slice(4,24);
    return r;
  }

}
