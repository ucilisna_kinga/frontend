import {Injectable} from '@angular/core';

@Injectable()
export class ThreadTmpService {

  threads = [
    {
      id: 0,
      forUser: { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  } ,
      participants: [
        { username: 'teacherFemale' , firstName: 'УчителкаИме', lastName: 'УчителкаПрезиме', gender: 'female' , profileType: 'teacher'  }
      ],
      relatedThreads: [1]
    },
    {
      id: 1,
      forUser: {username: 'teacherFemale', firstName: 'УчителкаИме', lastName: 'УчителкаПрезиме', gender: 'female', profileType: 'teacher'},
      participants: [
        { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  }
      ],
      relatedThreads: [0]
    },
    {
      id: 2,
      forUser: { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  } ,
      participants: [
        { username: 'studentFemale' , firstName: 'УченичкаИме', lastName: 'УченичкаПрезиме' , gender: 'female', profileType: 'student'  } ,
      ],
      relatedThreads: [3]
    },
    {
      id: 3,
      forUser: {username: 'studentFemale', firstName: 'УченичкаИме', lastName: 'УченичкаПрезиме', gender: 'female', profileType: 'student'},
      participants: [
        { username: 'jshalamanoski' , firstName: 'Јован', lastName: 'Шаламаноски' , gender: 'male', profileType: 'teacher'  }
      ],
      relatedThreads: [2]
    }
  ];

  activeThread = this.threads[0];

  getThreadsByUser(user) {
    const threads = [];
    for (const t of this.threads) {
      if (t.forUser.username === user.username) {
        threads.push(t);
      }
    }
    return threads;
  }

  getActiveThread() {
    return this.activeThread;
  }

  setActiveThread(thread) {
    this.activeThread = thread;
  }



}
