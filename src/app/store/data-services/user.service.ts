import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {AuthTmpService} from '../../core/services/auth/auth-tmp-service';
import {environment} from './../../../environments/environment';

import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  // private url =  'http://localhost:8081/';
  // private url =  'http://localhost:8081/api/user/';
  // private url =  'http://178.238.235.229:8080/api/user/';
  private url = environment.userServiceIpAddress;
  oldAllPostsFromFriendsLength = -1;

  constructor(private http: Http, private authTmpService: AuthTmpService) { }

  getUser() {
    // console.log('this.authTmpService.getToken()');
    // console.log(this.authTmpService.getToken());
    return this.http.get(this.url + 'user', { headers: this.getHeaders(this.authTmpService.getToken())})
      .map(res => res.json());
  }

  getUserByUsername(username) {
    return this.http.get(this.url + `userByUsername/${username}`, { headers: this.getHeaders(this.authTmpService.getToken())})
      .map(res => res.json());
  }

  searchUsers(searchTerm) {
    return this.http.get(this.url + `searchUsers/${searchTerm}`, {
      headers: this.getHeaders(this.authTmpService.getToken())
    }).map(res => res.json());
  }

  getUserFriends() {
    return this.http.get(this.url + 'friends', { headers: this.getHeaders(this.authTmpService.getToken())} )
      .map(res => res.json());
  }

  getUserFriendsByUsername(username) {
    return this.http.get(this.url + `friends/${username}`, { headers: this.getHeaders(this.authTmpService.getToken())} )
      .map(res => res.json());
  }

  addFriend(friendUsername) {
    return this.http.put(this.url + 'friend', { username: friendUsername} , {
      headers: this.getHeaders(this.authTmpService.getToken())
    }).map(res => res.json());
  }

  acceptFriendship(friendUsername) {
    return this.http.post(this.url + 'friend', { username: friendUsername} , {
      headers: this.getHeaders(this.authTmpService.getToken())
    }).map(res => res.json());
  }

  // delete friendship also used for refusing, decline friendship
  deleteUserFriend(friendUsername) {
    const params: URLSearchParams = new URLSearchParams();
    params.set('username', friendUsername);

    return this.http.delete(this.url + `friend/${friendUsername}`, {
        headers: this.getHeaders(this.authTmpService.getToken())
    })
      .map(res => res.json());
  }


  getFriendRequests(username) {
    return this.http.get(this.url + `friendsRequests/${username}`, { headers: this.getHeaders(this.authTmpService.getToken())} )
      .map(res => res.json());
  }


  getHeaders(token) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    return headers;
  }


  getUserTmp() {
    console.log('headers');
    console.log(this.getHeaders('eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDM2MDEzNjQsInVzZXJuYW1lIjoicGFyZW50TWFsZSJ9.MPiETivr_6MRwJiugHUUzJBtCAZnXy-YckQMnhJ3X5wZBzixZsHPPx4vH2w5J4Tng3NVAlnLs6Py4-cgauT_wQ'));
    return this.http.get(this.url + 'user', { headers: this.getHeaders('eyJhbGciOiJIUzUxMiJ9.eyJleHAiOjE1MDM2MDEzNjQsInVzZXJuYW1lIjoicGFyZW50TWFsZSJ9.MPiETivr_6MRwJiugHUUzJBtCAZnXy-YckQMnhJ3X5wZBzixZsHPPx4vH2w5J4Tng3NVAlnLs6Py4-cgauT_wQ')})
      .map(res => res.json());
  }


}
