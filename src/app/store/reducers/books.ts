
import {Book} from '../models/db-models/book';
import * as book from '../actions/book-actions';

export interface State {
  selectedBook: Book;
  booksFromSearch: Book[];
};

export const initialState: State = {
  selectedBook: { id: '', volumeInfo: { title: '', description: ''}  },
  booksFromSearch: []
};

export function reducer(state = initialState, action: book.Actions ): State {
  switch (action.type) {

    case book.SEARCH_COMPLETE: {
      return {
        selectedBook: state.selectedBook ,
        booksFromSearch: action.payload };
    }

    default: {
      return state;
    }
  }
}
