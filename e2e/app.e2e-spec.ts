import { SchoolBookPage } from './app.po';

describe('school-book App', () => {
  let page: SchoolBookPage;

  beforeEach(() => {
    page = new SchoolBookPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
